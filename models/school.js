module.exports = (sequelize, DataTypes) => {
  const School = sequelize.define('School', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    communityId: DataTypes.INTEGER,
    communityName: DataTypes.STRING,
    systemId: DataTypes.INTEGER,
    teachers: DataTypes.INTEGER,
    students: DataTypes.INTEGER,
    femaleStudents: DataTypes.INTEGER,
    maleStudents: DataTypes.INTEGER,
    system: DataTypes.STRING,
    haveSystem: DataTypes.STRING,
    improvedFacilitiesTypeI: DataTypes.INTEGER,
    improvedFacilitiesTypeIi: DataTypes.INTEGER,
    installationsWaterSoap: DataTypes.INTEGER,
    separateFacilitiesStaffStudents: DataTypes.STRING,
    separateFacilitiesBoysGirls: DataTypes.STRING,
    sufficientWashingFacilities: DataTypes.STRING,
    separateFacilitiesStaffStudentsHygiene: DataTypes.STRING,
    ecsEsc: DataTypes.DOUBLE,
    ecsEagi: DataTypes.DOUBLE,
    ecsShei: DataTypes.DOUBLE,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  School.associate = function(models) {
    School.belongsTo(models.Community, {
      foreignKey: 'community_id',
      as: 'communities',
    });
    School.belongsTo(models.System, {
      foreignKey: 'system_id',
      as: 'systems',
    });
  };

  School.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return School;
};
