module.exports = (sequelize, DataTypes) => {
  const HealthCenter = sequelize.define('HealthCenter', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    communityId: DataTypes.INTEGER,
    communityName: DataTypes.STRING,
    systemId: DataTypes.INTEGER,
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    pictureUrl: DataTypes.STRING,
    staff: DataTypes.INTEGER,
    patients: DataTypes.INTEGER,
    femalePatients: DataTypes.INTEGER,
    malePatients: DataTypes.INTEGER,
    system: DataTypes.STRING,
    haveSystem: DataTypes.STRING,
    improvedFacilitiesTypeI: DataTypes.INTEGER,
    improvedFacilitiesTypeIi: DataTypes.INTEGER,
    installationsWaterSoap: DataTypes.INTEGER,
    separateFacilitiesStaffPatients: DataTypes.STRING,
    separateFacilitiesBoysGirls: DataTypes.STRING,
    sufficientWashingFacilities: DataTypes.STRING,
    separateFacilitiesStaffPatientsHygiene: DataTypes.STRING,
    higcsi: DataTypes.DOUBLE,
    ecsCagi: DataTypes.DOUBLE,
    ecsShsi: DataTypes.DOUBLE,
    ecsCsa: DataTypes.DOUBLE,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  HealthCenter.associate = function(models) {
    HealthCenter.belongsTo(models.Community, {
      foreignKey: 'community_id',
      as: 'communities',
    });
    HealthCenter.belongsTo(models.System, {
      foreignKey: 'system_id',
      as: 'systems',
    });
  };

  HealthCenter.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return HealthCenter;
};
