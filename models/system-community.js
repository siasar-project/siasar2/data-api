module.exports = (sequelize, DataTypes) => {
  const SystemCommunity = sequelize.define('SystemCommunity', {
    systemId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    communityId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    servedHouseholds: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    underscored: true,
  });
  return SystemCommunity;
};
