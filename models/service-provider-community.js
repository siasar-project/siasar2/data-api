module.exports = (sequelize, DataTypes) => {
  const ServiceProviderCommunity = sequelize.define('ServiceProviderCommunity', {
    serviceProviderId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    communityId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    servedHouseholds: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    underscored: true,
  });
  return ServiceProviderCommunity;
};
